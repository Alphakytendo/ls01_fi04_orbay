﻿import java.util.Scanner;

class Fahrkartenautomat {

	public static double fahrkartenbestellungErfassen() {

		// Anzahl der Fahrkarten
		// --------------------

		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0;
		System.out.print("Zu zahlender Betrag (EURO): ");
		double zuZahlenderBetrag = tastatur.nextDouble();
		if (zuZahlenderBetrag <= 0) {
			zuZahlenderBetrag = 1;
			System.out.println("Ticketpreis kann nicht negativ oder 0 sein!\n Preis wurde auf 1 gesetzt\n");
		}

		System.out.print("Anzahl der Fahrkarten auswählen: ");
		int Kartenanzahl = tastatur.nextInt();
		if (Kartenanzahl > 10) {
			Kartenanzahl = 1;
			System.out.println(
					"Es können nicht mehr als 10 Tickets bestellt werden !\n Anzahl der Tickets auf 1 gesetzt!\n");
		}

		return zuZahlenderBetrag * Kartenanzahl;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		// Geldeinwurf
		// -----------
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen:  %5.2f Euro\n", (zuZahlenderBetrag) - eingezahlterGesamtbetrag);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		double rückgabebetrag = eingezahlterGesamtbetrag - (zuZahlenderBetrag);
		return rückgabebetrag;
	}

	public static void fahrkartenAusgeben() {
		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 25; i++) {
			System.out.print("=");
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

	}

	public static void rueckgeldAusgeben(double rückgabebetrag) {
		// Rückgeldberechnung und -Ausgabe
		// -------------------------------

		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.00) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		double rückgabebetrag;
		int Kartenanzahl;

		zuZahlenderBetrag = fahrkartenbestellungErfassen();
		rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(rückgabebetrag);

	}

}